#!/bin/bash

####################
get_val() { 
####################
	# Retrieve specific output parameter line from inspect/stats output
	FOUND=`echo "$OUT" | grep "$1:"`
	shift 1; COL="$1";

	# For multi-column results such as the docker stats output, use "alt" to ask for the 4th column
	[[ $COL == *"alt"*  ]] && { VAL=`echo "$FOUND" | cut -d' ' -f4`; } || { VAL=`echo "$FOUND" | cut -d' ' -f2`; }

	# If dealing with units in the result, convert the value into numeric GB representation
	[[ $COL == *"inGB"* ]] && { get_GB "$VAL"; } || { echo "$VAL"; }
}

####################
get_GB() {
####################
	# Convert input value into GB. The arcane knowledge of human-readable form that was lost by Docker
	case "$*" in 
		*GB*) echo "$*" | sed 's/GB//' ;;
		*PB*) IN=`echo "$*" | sed 's/PB//'`; bc <<< "scale=2; $IN * 1024 * 1024" ;;
		*TB*) IN=`echo "$*" | sed 's/TB//'`; bc <<< "scale=2; $IN * 1024" ;;
		*MB*) IN=`echo "$*" | sed 's/MB//'`; bc <<< "scale=2; $IN / 1024" ;;
		*B*)  IN=`echo "$*" | sed 's/B//'`;  bc <<< "scale=2; $IN / (1024 * 1024)" ;; esac
}

####################
get_uptime() { START=$(date -d `get_val StartedAt` +"%s"); NOW=$(date +"%s"); echo "$(( $NOW - $START ))"; }
####################

####################
get_status() {
####################
	# Set Status == 0 if Running and Status == 1 otherwise
	[[ "$(get_val Running)" == "true" ]] && { echo -n "Status\t0"; } || { echo -n "Status\t1"; }   
	
	# Create a Message when specific states are triggered
	STATES=( "Paused" "Restarting" "OOMKilled" "Dead" ); FLAGS=""
	for state in "${STATES[@]}"; do [[ "$(get_val $state)" == "true" ]] && { FLAGS="$FLAGS $state"; }; done

	# If container is not running then determine state and associated error
	[[ "$(get_val Running)" == "false" ]] && { echo -n -e "\tMessage\tFlags: $FLAGS; Error: $(get_val Error)"; }

	echo -n -e "\tRestartCount\t$(get_val RestartCount)\tMaxRetryCount\t$(get_val MaxRetryCount)\tExitCode\t$(get_val ExitCode)"
}

######################
###### Main ##########

# Verify that docker and version are available
which docker 2>&1 >/dev/null
[[ ! $? -eq 0 ]] && { exit 1; }

VERSION=`docker version -f '{{ .Server.Version }}'`
[[ ! $? -eq 0 ]] && { exit 1; }

# Docker parameters to extract for each running container
CONFIG='ContainerName: {{ .Name }}
Hostname: {{ .Config.Hostname }}
Status: {{ .State.Status }}
Running: {{ .State.Running }}
Paused: {{ .State.Paused }}
Restarting: {{ .State.Restarting }}
OOMKilled: {{ .State.OOMKilled }}
Dead: {{ .State.Dead }}
ExitCode: {{ .State.ExitCode }}
Error: {{ .State.Error }}
Created: {{ .Created }}
StartedAt: {{ .State.StartedAt }}
FinishedAt: {{ .State.FinishedAt }}
RestartCount: {{ .RestartCount }}
MaxRetryCount: {{ .HostConfig.RestartPolicy.MaximumRetryCount }}
Runtime: {{ .HostConfig.Runtime }}
Memory: {{ .HostConfig.Memory }}
ConfigImage: {{ .Config.Image }}
Platform: {{ .Platform }}
ImageVendor: {{ index .Config.Labels "org.label-schema.vendor" }}
ImageBuildDate: {{ index .Config.Labels "org.label-schema.build-date" }}
ResolvConfPath: {{ .ResolvConfPath }}
IPAddress: {{ .NetworkSettings.Networks.docknet.IPAddress }}
GlobalIPv6Address: {{ .NetworkSettings.Networks.docknet.GlobalIPv6Address }}
MacAddress: {{ .NetworkSettings.Networks.docknet.MacAddress }}'

# Running container metrics
STATS='CPUPerc: {{ .CPUPerc }}
NetIO: {{ .NetIO }}
BlockIO: {{ .BlockIO }}
MemPerc: {{ .MemPerc }}'

for cont in `docker ps -q`; do

	# Retrieve running container configuration
	OUT=$(docker inspect $cont --format "$CONFIG")
	[[ ! $? -eq 0 ]] && { exit 1; }

	# Query for running container metrics
	OUT=`echo -e "$OUT\n$(docker stats $cont --no-stream --format "$STATS")"`
	[[ ! $? -eq 0 ]] && { exit 1; }

	# Construct the output string
	echo -e "$(get_val ContainerName | sed 's%/%%g')\t$(get_status)\t\
ContainerEngine\tdocker\t\
ContainerEngineVer\t$VERSION\t\
Runtime\t$(get_val Runtime)\t\
Created\t$(date -d `get_val Created` +"%s")\t\
StartedAt\t$(date -d `get_val StartedAt` +"%s")\t\
FinishedAt\t$(date -d `get_val FinishedAt` +"%s")\t\
Uptime\t$(get_uptime)\t\
CPUPercent\t$(get_val CPUPerc | sed 's/%//g')\t\
MemPercent\t$(get_val MemPerc | sed 's/%//g')\t\
MaxMemoryB\t$(get_val Memory)\t\
NetIN_GB\t$(get_val NetIO inGB)\t\
NetOUT_GB\t$(get_val NetIO alt-inGB)\t\
BlockIN_GB\t$(get_val BlockIO inGB)\t\
BlockOUT_GB\t$(get_val BlockIO alt-inGB)\t\
Platform\t$(get_val Platform)\t\
Image\t$(get_val ConfigImage)\t\
ImageVendor\t$(get_val ImageVendor)\t\
ImageBuildDate\t$(date -d `get_val ImageBuildDate` +"%s")\t\
IPAddress\t$(get_val IPAddress)\t\
IPv6Address\t$(get_val GlobalIPv6Address)\t\
MacAddress\t$(get_val MacAddress)"
done
echo "DONE"
